.. CASPEN Documentation documentation master file, created by
   sphinx-quickstart on Mon Feb  8 10:26:36 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SIMCADEMY Documentation
=======================

Contents:

.. toctree::
   :maxdepth: 3

   documentation/intro
   documentation/blockMesh/blockMesh
   documentation/controlDictionary/controlDictionary
   documentation/transportProperties/transportProperties
